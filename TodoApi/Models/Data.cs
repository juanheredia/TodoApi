﻿using System;
using Newtonsoft.Json;

namespace TodoApi.Models
{
    public class CounterData
    {
        public int LastUpdateInSeconds { get; set; }
        public DateTime LastUpdate { get; set; }
        public int Total { get; set; }
    }

    public class CarState
    {
        private double _Odometer;
        public bool IsOn { get; set; }
        public bool IsCharging { get; set; }
        public double ChargerCurrent { get; set; }
        public double EstimatedOdometer
        {
            get
            {
                if (_Odometer == 0)
                {
                    _Odometer = Tools.GetLastOdometer();
                }
                return _Odometer;
            }
            set
            {
                _Odometer = value;
                Tools.SetLastOdometer(_Odometer);
            }
        }

        // Calculate Attributes
        public bool ShouldSend2ABRP
        {
            get
            {
                // return IsOn || (IsCharging && ChargerCurrent > 0);
                // Always send data to ABRP unless it´s a recovery
                return !(IsCharging && ChargerCurrent <= 0);
            }
        }
    }

    public class GlobalSettings
    {
        public string ABRPUrl { get; set; }
        public string ABRP_api_key { get; set; }
        public string ABRP_token { get; set; }
        public string OVMSUrl { get; set; }
        public string OVMSid { get; set; }
        public string OVMSpass { get; set; }
        public string CAR_MODEL { get; set; }
        public double CAR_BATTERY { get; set; }
        public double FIX_DISTANCE { get; set; }
        public bool DebugMode { get; set; }
        public bool EnviromentUAT { get; set; }
        public int TimerSeconds { get; set; }
        public string InfluxDBToken { get; set; }
        public string InfluxDBUser { get; set; }
        public string InfluxDBDataBase { get; set; }
        public string InfluxDBServer { get; set; }
        public string CLOUDMQTT_SERVER { get; set; }
        public int CLOUDMQTT_PORT { get; set; }
        public string CLOUDMQTT_USER { get; set; }
        public string CLOUDMQTT_PASSWORD { get; set; }
        public string MQTT_BASETOPIC { get; set; }
        public string HomeAssistantServer { get; set; }
        public string HomeAssistantToken { get; set; }
        public string TeslaMateConnectionString { get; set; }
        public string APIToken { get; set; }
    }

    public class tlm
    {
        // {"utc":1596007220,"soc":76,"soh":100,"speed":0,"car_model":"hyundai:kona:19:39:other",
        // "lat":"40.346","lon":"-3.676","alt":"587.8",
        // "ext_temp":29.5,"is_charging":0,"batt_temp":33,"voltage":355.5,"current":0.3,"power":"0.1"}


        // {"is_charging": no, "lat": 40.3458, "lon": -3.67613, "alt": 585, "soc": 70.5, "soh": 100,
        // "speed": , "ext_temp": 25.5, "batt_temp": 27, "voltage": 349.2,"current": 0.1, "power": 0.03492}

        private double _soc;
        private double _soh;
        public string car_model { get; set; }
        public int utc { get; set; }
        public int intSOC { get; set; }
        public double power { get; set; }
        public int speed { get; set; }
        public double direction { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
        public bool is_charging { get; set; }
        public bool is_dcfc { get; set; }
        public bool is_parked { get; set; }
        public double alt { get; set; }
        public int intSOH { get; set; }
        public double ext_temp { get; set; }
        public double batt_temp { get; set; }
        public double voltage { get; set; }
        public double current { get; set; }
        public int odometer { get; set; }

        // A field that is not serialized.
        [JsonIgnore]
        public double estimatedodometer { get; set; }
        [JsonIgnore]
        public double distance { get; set; }
        [JsonIgnore]
        public double soc
        {
            get { return _soc; }
            set
            {
                _soc = value;
                intSOC = Convert.ToInt32(Math.Floor(_soc));
                // intSOC = Convert.ToInt32(_soc);
            }
        }
        [JsonIgnore]
        public double soh
        {
            get { return _soh; }
            set
            {
                _soh = value;
                intSOH = Convert.ToInt32(Math.Floor(_soh));
                // intSOH = Convert.ToInt32(_soh);
            }
        }

        public void setData(string Topic, string Value)
        {
            var BaseTopic = Program.AppConfig.MQTT_BASETOPIC;
            if (Topic.Equals(BaseTopic + "v/p/latitude")) lat = Convert.ToDouble(Value);
            else if (Topic.Equals(BaseTopic + "v/p/longitude")) lon = Convert.ToDouble(Value);
            else if (Topic.Equals(BaseTopic + "v/p/altitude")) alt = Convert.ToDouble(Value);
            else if (Topic.Equals(BaseTopic + "v/b/soc")) soc = Convert.ToDouble(Value);
            else if (Topic.Equals(BaseTopic + "v/b/soh")) soh = Convert.ToDouble(Value);
            else if (Topic.Equals(BaseTopic + "v/p/direction")) direction = Convert.ToDouble(Value);
            else if (Topic.Equals(BaseTopic + "v/e/temp")) ext_temp = Convert.ToDouble(Value);
            else if (Topic.Equals(BaseTopic + "v/b/temp")) batt_temp = Convert.ToDouble(Value);
            else if (Topic.Equals(BaseTopic + "v/b/voltage")) voltage = Convert.ToDouble(Value);
            else if (Topic.Equals(BaseTopic + "v/b/power")) power = Convert.ToDouble(Value);
            else if (Topic.Equals(BaseTopic + "v/e/on")) Program.carState.IsOn = Value.Equals("yes");
            else if (Topic.Equals(BaseTopic + "v/e/parktime")) is_parked = (Convert.ToInt32(Value) > 0);
            else if (Topic.Equals(BaseTopic + "v/p/speed")) speed = Convert.ToInt32(Value);
            else if (Topic.Equals(BaseTopic + "v/p/odometer"))
            {
                // Respect this valor even for EstimatedOdometer
                var AuxOdometer = Convert.ToDouble(Value);
                if (AuxOdometer > 0)
                {
                    odometer = Convert.ToInt32(AuxOdometer);
                    Program.carState.EstimatedOdometer = AuxOdometer;
                }

            }
            else if (Topic.Equals(BaseTopic + "v/b/current"))
            {
                current = Convert.ToDouble(Value);
                Program.carState.ChargerCurrent = current * -1;
            }
            else if (Topic.Equals(BaseTopic + "v/c/state"))
            {
                is_charging = Value.Equals("charging") || Value.Equals("topoff");
                Program.carState.IsCharging = is_charging;
                if (is_charging) is_dcfc = (Program.carState.ChargerCurrent >= 7.2);
            }
        }

        public bool isEqual(tlm objCompare)
        {
            return
                this.soc == objCompare.soc &&
                this.power == objCompare.power &&
                this.speed == objCompare.speed &&
                this.direction == objCompare.direction &&
                this.lat == objCompare.lat &&
                this.lon == objCompare.lon &&
                this.is_charging == objCompare.is_charging &&
                this.is_dcfc == objCompare.is_dcfc &&
                this.is_parked == objCompare.is_parked &&
                this.alt == objCompare.alt &&
                this.soh == objCompare.soh &&
                this.ext_temp == objCompare.ext_temp &&
                this.batt_temp == objCompare.batt_temp &&
                this.voltage == objCompare.voltage &&
                this.current == objCompare.current;
        }

        public tlm ShallowCopy()
        {
            return (tlm)this.MemberwiseClone();
        }
    }
}