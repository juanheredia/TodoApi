using System;
using System.Threading.Tasks;
using InfluxDB.Client;
using Microsoft.Extensions.Configuration;

namespace TodoApi.Models
{
    public class InfluxDBService
    {
        private readonly string _token;

        public InfluxDBService(IConfiguration configuration)
        {
            _token = Program.AppConfig.InfluxDBToken;
        }

        public void Write(Action<WriteApi> action)
        {
            using var client = InfluxDBClientFactory.Create(Program.AppConfig.InfluxDBServer, _token);
            using var write = client.GetWriteApi();
            action(write);
        }

        public async Task<T> QueryAsync<T>(Func<QueryApi, Task<T>> action)
        {
            using var client = InfluxDBClientFactory.Create(Program.AppConfig.InfluxDBServer, _token);
            var query = client.GetQueryApi();
            return await action(query);
        }
    }
}