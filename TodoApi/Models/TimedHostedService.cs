﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace TodoApi.Models
{
    public class TimedHostedService : IHostedService, IDisposable
    {

        private Timer _timer;
        //private bool _isSending2ABR;
        private object lockObject = new object(); // Just an object to wait DoWork complete the task


        public Task StartAsync(CancellationToken cancellationToken)
        {
            Models.Tools.guardarLog("Timed Background Service is starting.");
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(Program.AppConfig.TimerSeconds));
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            if (Monitor.TryEnter(lockObject))
            {
                try
                {
                    Tools.SaveAndSendData();
                }
                catch (Exception ex)
                {
                    Tools.guardarLog("DoWork Error: " + ex.Message);
                }
                finally
                {
                    Monitor.Exit(lockObject);
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Tools.guardarLog("Timed Background Service is stopping.");
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
