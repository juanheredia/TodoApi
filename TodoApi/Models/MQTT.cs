﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Client.Options;
using MQTTnet.Client.Receiving;

namespace TodoApi.Models
{
    public interface IMqttClientService : IHostedService,
            IMqttClientConnectedHandler,
            IMqttClientDisconnectedHandler,
            IMqttApplicationMessageReceivedHandler
    {
    }

    public class MqttClientService : IMqttClientService
    {
        private IMqttClient mqttClient;
        private IMqttClientOptions options;
        private static double lastOdometer;

        public MqttClientService(IMqttClientOptions options)
        {
            this.options = options;
            mqttClient = new MqttFactory().CreateMqttClient();
            ConfigureMqttClient();
        }

        private void ConfigureMqttClient()
        {
            mqttClient.ConnectedHandler = this;
            mqttClient.DisconnectedHandler = this;
            mqttClient.ApplicationMessageReceivedHandler = this;
        }


        public async Task HandleApplicationMessageReceivedAsync(MqttApplicationMessageReceivedEventArgs eventArgs)
        {
            var Value = Encoding.UTF8.GetString(eventArgs.ApplicationMessage.Payload);
            //if (Program.AppConfig.DebugMode)
            //{
            //    Tools.guardarLog($"+ Topic = {eventArgs.ApplicationMessage.Topic}");
            //    Tools.guardarLog($"+ Payload = {Value}");
            //    Tools.guardarLog($"+ QoS = {eventArgs.ApplicationMessage.QualityOfServiceLevel}");
            //    Tools.guardarLog($"+ Retain = {eventArgs.ApplicationMessage.Retain}");
            //}

            // Now we will receive all data an need to parse.
            // Watch if odometer received
            Program.currentTLM.setData(eventArgs.ApplicationMessage.Topic, Value);

            // Publish the estimated odometer
            if (Program.carState.EstimatedOdometer != lastOdometer)
            {
                var Topic = "v/p/estimatedodometer";
                await Publish(Topic, Convert.ToString(Program.carState.EstimatedOdometer));
                lastOdometer = Program.carState.EstimatedOdometer;
            }
            await Task.CompletedTask;
        }


        public async Task HandleConnectedAsync(MqttClientConnectedEventArgs eventArgs)
        {
            var BaseTopic = Program.AppConfig.MQTT_BASETOPIC;
            Tools.guardarLog("MQTT Server Connected");

            // Subscribe Topics
            //await mqttClient.SubscribeAsync(BaseTopic + "v/#");
            //await mqttClient.SubscribeAsync("abrp/status");
            //await mqttClient.SubscribeAsync(BaseTopic + "v/c/charging");
            //await mqttClient.SubscribeAsync(BaseTopic + "v/c/current");
            await mqttClient.SubscribeAsync(BaseTopic + "v/e/on");
            await mqttClient.SubscribeAsync(BaseTopic + "v/p/latitude");
            await mqttClient.SubscribeAsync(BaseTopic + "v/p/longitude");
            await mqttClient.SubscribeAsync(BaseTopic + "v/p/altitude");
            await mqttClient.SubscribeAsync(BaseTopic + "v/b/soc");
            await mqttClient.SubscribeAsync(BaseTopic + "v/b/soh");
            await mqttClient.SubscribeAsync(BaseTopic + "v/p/speed");
            await mqttClient.SubscribeAsync(BaseTopic + "v/e/temp");
            await mqttClient.SubscribeAsync(BaseTopic + "v/b/temp");
            await mqttClient.SubscribeAsync(BaseTopic + "v/b/voltage");
            await mqttClient.SubscribeAsync(BaseTopic + "v/b/current");
            await mqttClient.SubscribeAsync(BaseTopic + "v/b/power");
            await mqttClient.SubscribeAsync(BaseTopic + "v/c/state");
            await mqttClient.SubscribeAsync(BaseTopic + "v/p/odometer");
            await mqttClient.SubscribeAsync(BaseTopic + "v/e/parktime");
            await mqttClient.SubscribeAsync(BaseTopic + "v/p/direction");
        }

        public async Task Publish(string Topic, string Payload)
        {
            Topic = Program.AppConfig.MQTT_BASETOPIC + Topic;
            await mqttClient.PublishAsync(Topic, Payload);
        }
        public async Task HandleDisconnectedAsync(MqttClientDisconnectedEventArgs eventArgs)
        {
            Tools.guardarLog("MQTT Server Disconnected");
            int i = 0;
            while (!mqttClient.IsConnected)
            {
                i++;
                await Task.Delay(TimeSpan.FromSeconds(5 * i));
                try
                {
                    await mqttClient.ConnectAsync(options, CancellationToken.None); // Since 3.0.5 with CancellationToken
                }
                catch
                {
                    Tools.guardarLog("MQTT Server could not reconnect");
                }
            }
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await mqttClient.ConnectAsync(options);
            if (!mqttClient.IsConnected)
            {
                await mqttClient.ReconnectAsync();
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                var disconnectOption = new MqttClientDisconnectOptions
                {
                    ReasonCode = MqttClientDisconnectReason.NormalDisconnection,
                    ReasonString = "NormalDiconnection"
                };
                await mqttClient.DisconnectAsync(disconnectOption, cancellationToken);
            }
            await mqttClient.DisconnectAsync();
        }


    }
}
