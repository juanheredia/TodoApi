﻿using InfluxDB.Client;
using InfluxDB.Client.Api.Domain;
using InfluxDB.Client.Writes;
using Microsoft.AspNetCore.Mvc;
using MQTTnet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;

namespace TodoApi.Models
{
    public class Tools
    {
        private static string counterFile;
        private static string odometerFile;
        private static tlm lastTLM;
        private static DateTime lastTimeChanged;

        // This is for Thread Safe File Writing
        // To avoid this kind of Errors: The process cannot access the file '{path}' because it is being used by another process
        private static ReaderWriterLockSlim _readWriteLock = new ReaderWriterLockSlim();

        public async static void MqttPublish(string Topic, string Payload)
        {
            var client = new MqttFactory().CreateMqttClient();
            var mqtt = new MqttFactory().CreateLowLevelMqttClient();
            var options = new MQTTnet.Client.Options.MqttClientOptionsBuilder()
                .WithClientId(Environment.MachineName)
                .WithTcpServer(Program.AppConfig.CLOUDMQTT_SERVER, Program.AppConfig.CLOUDMQTT_PORT)
                .WithCredentials(Program.AppConfig.CLOUDMQTT_USER, Program.AppConfig.CLOUDMQTT_PASSWORD)
                .Build();
            await client.ConnectAsync(options, CancellationToken.None);
            var message = new MqttApplicationMessageBuilder()
                .WithTopic(Topic)
                .WithPayload(Payload)
                .WithAtLeastOnceQoS()
                .Build();
            await client.PublishAsync(message, CancellationToken.None);
        }
        public static bool ValidateToken(string Token)
        {
            return Token.Equals("Bearer " + Program.AppConfig.APIToken);
        }

        public static void guardarLog(string Texto)
        {
            // Set Status to Locked
            _readWriteLock.EnterWriteLock();

            try
            {
                using (StreamWriter w = System.IO.File.AppendText(FicheroLog()))
                {
                    w.WriteLine("{0} - {1}", DateTime.Now.ToString(), Texto);
                    w.Flush();
                    w.Close();
                }
            }
            finally
            {
                // Release lock
                _readWriteLock.ExitWriteLock();
            }
        }

        public static void SaveTextInFile(string Texto, string File)
        {
            // Set Status to Locked
            _readWriteLock.EnterWriteLock();

            try
            {
                System.IO.File.WriteAllText(File, Texto);
            }
            finally
            {
                // Release lock
                _readWriteLock.ExitWriteLock();
            }
        }

        private static string FicheroLog()
        {
            string Dir = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "logs");

            if (!Directory.Exists(Dir))
            {
                Directory.CreateDirectory(Dir);
            }
            string fichero = Path.Combine(Dir, DateTime.Today.ToString("yyyyMMdd") + ".log");
            if (!System.IO.File.Exists(fichero))
            {
                System.IO.File.Create(fichero).Close();
            }
            return fichero;
        }

        public static ActionResult<string> SendData2OVMS(string command)
        {
            // List of Posible commands

            string resultContent;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Program.AppConfig.OVMSUrl);
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("noidsave", ""),
                    new KeyValuePair<string, string>("_mod", "ovms/cmd"),
                    new KeyValuePair<string, string>("_vehicleid", Program.AppConfig.OVMSid),
                    new KeyValuePair<string, string>("_carpass", Program.AppConfig.OVMSpass),
                    new KeyValuePair<string, string>("_cmd", command),
                });
                var response = client.PostAsync("", content);
                resultContent =
                    response.Result.Content.ReadAsStringAsync().Result.Replace("<span>", string.Empty).Replace("</span>", string.Empty).Trim();

                if (!response.IsCompletedSuccessfully)
                {
                    resultContent = "Error sending to OVMS: " + resultContent;
                    Models.Tools.guardarLog(resultContent);
                }
                else
                {
                    Models.Tools.guardarLog("Command sent to OVMS: " + command);
                    Models.Tools.guardarLog("Result: " + resultContent);
                    // Parse result (if we get a lot of data, just return a short response)
                    if (resultContent.Contains("bat temp"))
                    {
                        string[] lines = resultContent.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                        var newresult = string.Empty;
                        foreach (var line in lines)
                        {
                            if (!line.Contains("="))
                            {
                                newresult += line;
                            }
                        }
                        resultContent = newresult;
                    }
                }

            }
            return resultContent;
        }

        public static DateTime getCounterFileDate()
        {
            //InitCounter();
            return System.IO.File.GetLastWriteTime(counterFile);
        }

        public static int AddCount()
        {
            var i = GetCounter();
            i++;
            SetCounter(i);
            return i;
        }

        public static tlm parseTLM(string tlm)
        {
            tlm myJsonObject = null;
            try
            {
                myJsonObject = JsonConvert.DeserializeObject<tlm>(tlm);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += " | " + ex.InnerException.Message;
                }
                guardarLog(msg);
            }

            return myJsonObject;
        }

        public static string serializeReturnTLM(tlm tlm)
        {
            string newTLM = JsonConvert.SerializeObject(tlm);
            return newTLM;
        }

        private static void SetCounter(int Counter)
        {
            //InitCounter();
            TextWriter tw = new StreamWriter(counterFile);
            // Write counter to file
            tw.WriteLine(Counter);


            // close the stream     
            tw.Close();
        }

        public static int GetCounter()
        {
            //InitCounter();

            TextReader tr = new StreamReader(counterFile);
            // read file
            var n = tr.ReadLine();
            // close the stream
            tr.Close();

            int Counter;
            if (!int.TryParse(n, out Counter)) Counter = 0;
            return Counter;
        }

        public static void SetLastOdometer(double Counter)
        {
            //InitCounter();
            TextWriter tw = new StreamWriter(odometerFile);
            // Write counter to file
            tw.WriteLine(Counter);
            // close the stream     
            tw.Close();
        }

        public static double GetLastOdometer()
        {
            TextReader tr = new StreamReader(odometerFile);
            // read file
            var n = tr.ReadLine();
            // close the stream
            tr.Close();

            double Counter;
            if (!double.TryParse(n, out Counter)) Counter = 0;
            return Counter;
        }

        public static GlobalSettings getConfig()
        {
            var configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "config.json");
            GlobalSettings config = null;
            using (StreamReader r = new StreamReader(configFile))
            {
                string json = r.ReadToEnd();
                config = JsonConvert.DeserializeObject<GlobalSettings>(json);
            }
            // Set aux files 
            var AuxPath = Path.GetDirectoryName(FicheroLog());
            counterFile = Path.Combine(AuxPath, "counter.txt");
            if (!System.IO.File.Exists(counterFile)) System.IO.File.Create(counterFile).Close();
            odometerFile = Path.Combine(AuxPath, "odometer.txt");
            if (!System.IO.File.Exists(odometerFile)) System.IO.File.Create(odometerFile).Close();

            lastTLM = new tlm();

            return config;
        }

        public static void SaveDataIntoInfluxDB(tlm objTLM)
        {
            //char[] Token = Program.AppConfig.InfluxDBToken.ToCharArray();
            //var influxDBClient = InfluxDBClientFactory.CreateV1(Program.AppConfig.InfluxDBServer,
            //    Program.AppConfig.InfluxDBUser, Token, Program.AppConfig.InfluxDBDataBase, null);

            string Token = Program.AppConfig.InfluxDBToken;
            var influxDBClient = InfluxDBClientFactory.Create(Program.AppConfig.InfluxDBServer, Token);

            using (var writeApi = influxDBClient.GetWriteApi())
            {

                // Let's calculate the distance between the previous point.
                //var distance = Models.PreviousData.getDistance(objTLM.lat, objTLM.lon, objTLM.alt);

                // Let's calculate Consumption kWh
                // Car Battery degradation
                var percentUtil = Program.AppConfig.CAR_BATTERY * objTLM.soh / 100;
                var ConsumptionkWh = Models.PreviousData.getDiffSOC(objTLM.soc) * percentUtil / 100;

                // Let's calculate Consumption kWh/100
                // Not Necesary, it's calculated in Grafana
                // var ConsumptionkWh100 = ConsumptionkWh / (distance / 1000) * 100;

                // Let's conver the UTC to datetime
                var timeFromOVMS = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(objTLM.utc * 1000.0);

                //
                // Write by Point
                //
                var point = PointData.Measurement("ovms")
                    .Tag("vehicle", "konaev")
                    .Field("alt", objTLM.alt)
                    .Field("batt_temp", objTLM.batt_temp)
                    .Field("current", objTLM.current)
                    .Field("ext_temp", objTLM.ext_temp)
                    .Field("is_charging", objTLM.is_charging)
                    .Field("lat", objTLM.lat)
                    .Field("lon", objTLM.lon)
                    .Field("power", objTLM.power)
                    .Field("soc", objTLM.soc)
                    .Field("soh", objTLM.soh)
                    .Field("speed", objTLM.speed)
                    .Field("utc", objTLM.utc)
                    .Field("distance", objTLM.distance)
                    .Field("consumptionkwh", ConsumptionkWh)
                    .Field("voltage", objTLM.voltage)
                    .Field("is_aux_recuperation", !Program.carState.ShouldSend2ABRP)
                    //.Field("Consumptionkwh100", ConsumptionkWh100)
                    .Timestamp(timeFromOVMS, WritePrecision.Ns);

                //writeApi.WritePoint(point);
                writeApi.WritePoint(Program.AppConfig.InfluxDBDataBase, Program.AppConfig.InfluxDBUser, point);

            }
            influxDBClient.Dispose();
        }

        //public static int SaveAndSendData(string tlm, bool Send2ABRP)
        public static int SaveAndSendData()
        {
            // In order to avoid operation in the current object, make a copy
            var currentTLM = Program.currentTLM.ShallowCopy();

            int Counter = 0;
            // Just Continue if GPS coordinates are different from 0
            if (currentTLM.lat == 0 && currentTLM.lon == 0 && currentTLM.alt == 0)
            {
                // There is no GPS data yet
                return 0;
            }
            // Verify if last data changed 
            // OR passed 10 minutes
            //var elapsedMinutes = DateTime.Now.Subtract(lastTimeChanged).TotalMinutes;
            var hasChanged = !lastTLM.isEqual(currentTLM);


            // To enable vehicle individual consumption calibration,
            // we need at least speed, power and is_charging at a rate of at least once per 10 seconds
            // (the faster the better).
            //if (!hasChanged && elapsedMinutes < 10)
            if (!hasChanged) // Only save data if there is some change. 
            {
                // No need to save and update data
                return 0;
            }
            // Fill utc and car model
            currentTLM.car_model = Program.AppConfig.CAR_MODEL;
            // utc must be send in every post
            currentTLM.utc = Convert.ToInt32(DateTime.UtcNow.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds / 1000);

            // Set Odometer and Distance
            if (Program.carState.EstimatedOdometer > 0 && currentTLM.speed > 0)
            {
                // The car is moving Calculate distance and add to last Odometer
                currentTLM.distance = Models.PreviousData.getDistance(Program.currentTLM.lat, Program.currentTLM.lon, Program.currentTLM.alt) * Program.AppConfig.FIX_DISTANCE;
                if (currentTLM.distance > 0)
                {
                    currentTLM.estimatedodometer = Program.carState.EstimatedOdometer += currentTLM.distance / 1000;
                }
            }
            currentTLM.odometer = Convert.ToInt32(Program.carState.EstimatedOdometer);
            lastTimeChanged = DateTime.Now;
            lastTLM = currentTLM;
            try
            {
                // Send data only when not on UAT
                if (Program.AppConfig.EnviromentUAT)
                {
                    string stringTLM = ComposeJSONString2Send(currentTLM);
                    Tools.guardarLog(stringTLM);
                }
                else
                {
                    // Send data to ABRP
                    // Only if it's not a recuperation
                    if (Program.carState.ShouldSend2ABRP)
                    {
                        Counter = Send2ABRP(currentTLM);
                    }
                    // Save data into InfluxDB
                    SaveDataIntoInfluxDB(currentTLM);
                }
            }
            catch (Exception ex)
            {
                string msg = "Source: " + ex.Source + " | " + ex.Message;
                if (ex.InnerException != null)
                {
                    msg += " | " + ex.InnerException.Message;
                }
                guardarLog(msg);
            }
            return Counter;
        }
        public static void updateABRPSensorOnHA(bool Estado)
        {
            string jsonData = "{" + string.Format("{1}state{1}: {0}", Estado ? 1 : 0, '"') + "}";
            SendData2HA("sensor.abrp", jsonData);

            // Models.Tools.guardarLog("Actual isSending2ABRP is: " + Program.carState.isSending2ABRP.ToString()
            //     + " | Received: isActive: " + isActive);

            StringBuilder msg = new StringBuilder();
            msg.Append(Estado ? "Start " : "Stop ");
            msg.AppendLine("Sending Data to ABRP.");

            Models.Tools.guardarLog(msg.ToString());
        }


        private static string ComposeJSONString2Send(tlm objTLM)
        {
            // Adjust soc and soh to be an Integer
            // objTLM.soc = Math.Floor(objTLM.soc);
            // objTLM.soh = Math.Floor(objTLM.soh);

            // Reparse TLM
            string stringTLM = serializeReturnTLM(objTLM);

            // Adjust JSON
            // change alt by elevation
            stringTLM = stringTLM.Replace("\"alt\"", "\"elevation\"");

            // change direcction by heading
            stringTLM = stringTLM.Replace("\"direction\"", "\"heading\"");

            // Rename intSOC and intSOH
            stringTLM = stringTLM.Replace("\"intSOC\"", "\"soc\"");
            stringTLM = stringTLM.Replace("\"intSOH\"", "\"soh\"");

            // if odometer is 0 don't include
            if (objTLM.odometer == 0)
            {
                stringTLM = stringTLM.Replace(",\"odometer\":0", "");
            }

            stringTLM = "{\"tlm\": " + $"{stringTLM}" + "}";
            return stringTLM;
        }

        private static int Send2ABRP(tlm objTLM)
        {
            string stringTLM = ComposeJSONString2Send(objTLM);
            if (Program.AppConfig.DebugMode) Models.Tools.guardarLog(stringTLM);

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "APIKEY " + Program.AppConfig.ABRP_api_key);
            string uri = $"{Program.AppConfig.ABRPUrl}?token={Program.AppConfig.ABRP_token}";
            var result = client.PostAsync(uri, new StringContent(stringTLM, Encoding.UTF8, "application/json")).Result;

            if (!result.IsSuccessStatusCode)
            {
                Models.Tools.guardarLog("Error sending to ABRP: " + result.ReasonPhrase + " | " + result.RequestMessage + " | " + result.ToString());
            }
            else
            {
                //var log = result.Version + " | " + result.ReasonPhrase + " | " + result.RequestMessage;
                var log = result.Version + " | " + result.ReasonPhrase;
                if (Program.AppConfig.DebugMode) Models.Tools.guardarLog(log);
                return Models.Tools.AddCount();
            }
            return 0;
        }

        private static void SendData2HA(string Sensor, string jsonInString)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Program.AppConfig.HomeAssistantToken);
            string uri = $"{Program.AppConfig.HomeAssistantServer}/api/states/{Sensor}";
            var response = client.PostAsync(uri, new StringContent(jsonInString, Encoding.UTF8, "application/json")).Result;
            if (!response.IsSuccessStatusCode)
            {
                //if (Program.AppConfig.DebugMode)
                //{
                Models.Tools.guardarLog("Error sending to HA: " + response.ToString());
                //}
            }
        }
    }
}
