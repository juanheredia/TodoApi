using Microsoft.AspNetCore.Mvc;
using System;
using Npgsql;
using System.Data;

namespace TodoApi.Controllers
{
    public class TeslamatePostgreSQL : Controller
    {
        public IActionResult Index()
        {
            return View("Index");
        }

        [HttpPost]
        public IActionResult Update()
        {
            //var Action = UpdateTeslamatePGData();
            var Action = FixTeslamateImport();
            if (Action) return View("Success");
            else return View("Error");
        }
        public bool UpdateTeslamatePGData()
        {
            //var ConnectionString = "Server=192.168.1.2;Port=5432;User Id=admin;Password=q5MPqDMa427M9QNf;Database=teslamate;";
            NpgsqlConnection connection = new NpgsqlConnection(Program.AppConfig.TeslaMateConnectionString);
            var senSQL = "SELECT MAX(end_date) FROM \"states\";";

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = senSQL;

            DateTime lastDate = DateTime.Now.AddYears(1); // Just a year more to be sure the difference is more than 0
            try
            {
                connection.Open();
                NpgsqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    lastDate = Convert.ToDateTime(dr[0]);
                    dr.Close();
                }

                // Calculate time Difference.
                int diffInSeconds = Convert.ToInt32((DateTime.Now - lastDate).TotalSeconds);
                if (diffInSeconds > 0)
                {
                    // Update Tables on Database  
                    //SELECT MAX(end_date)+ '1 second'::INTERVAL FROM "states"; 
                    // SELECT * FROM "states" WHERE id = 95 ;
                    // UPDATE "states" SET end_date = end_date - '100 second'::INTERVAL WHERE id = 95 ;

                    senSQL =
                        string.Format(
                            "UPDATE \"states\" SET start_date = start_date + '{0} second'::INTERVAL, end_date = end_date + '{0} second'::INTERVAL; " +
                            "UPDATE \"drives\" SET start_date = start_date + '{0} second'::INTERVAL, end_date = end_date + '{0} second'::INTERVAL; " +
                            "UPDATE \"charging_processes\" SET start_date = start_date + '{0} second'::INTERVAL, end_date = end_date + '{0} second'::INTERVAL; " +
                            "UPDATE \"positions\" SET date = date + '{0} second'::INTERVAL; " +
                            "UPDATE \"charges\" SET date = date + '{0} second'::INTERVAL; "
                            , diffInSeconds);
                    cmd.CommandText = senSQL;
                    var result = cmd.ExecuteNonQuery();
                    Models.Tools.guardarLog("Success UpdateTeslamatePGData: " + result.ToString() + " records.");
                }
                else
                {
                    Models.Tools.guardarLog("UpdateTeslamatePGData, no records to update.");
                }

            }
            catch (Exception ex)
            {
                Models.Tools.guardarLog("Error from UpdateTeslamatePGData: " + ex.Message);
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }

            //var fluxTables = await DateTime.Now.ToFileTimeUtc();
            return true;
        }

        public bool FixTeslamateImport()
        {
            NpgsqlConnection connection = new NpgsqlConnection(Program.AppConfig.TeslaMateConnectionString);
            // var senSQL = "SELECT MAX(end_date) FROM \"states\";";
            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandType = CommandType.Text;
            try
            {
                connection.Open();
                var seedDate = "'20190101'";
                var yearsToFix = 10;
                var senSQL =
                    string.Format(
                        "UPDATE \"states\" SET start_date = start_date + '{0} year'::INTERVAL, end_date = end_date + '{0} year'::INTERVAL WHERE start_date < {1}; " +
                        "UPDATE \"drives\" SET start_date = start_date + '{0} year'::INTERVAL, end_date = end_date + '{0} year'::INTERVAL WHERE start_date < {1}; " +
                        "UPDATE \"charging_processes\" SET start_date = start_date + '{0} year'::INTERVAL, end_date = end_date + '{0} year'::INTERVAL WHERE start_date < {1}; " +
                        "UPDATE \"positions\" SET date = date + '{0} year'::INTERVAL WHERE date < {1}; " +
                        "UPDATE \"charges\" SET date = date + '{0} year'::INTERVAL WHERE date < {1}; "
                        , yearsToFix, seedDate);
                cmd.CommandText = senSQL;
                var result = cmd.ExecuteNonQuery();
                Models.Tools.guardarLog("Success FixTeslamateImport: " + result.ToString() + " records.");
            }
            catch (Exception ex)
            {
                Models.Tools.guardarLog("Error from FixTeslamateImport: " + ex.Message);
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
            //var fluxTables = await DateTime.Now.ToFileTimeUtc();
            return true;
        }
    }
}