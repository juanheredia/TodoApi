﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { DateTime.Now.ToString(), Environment.MachineName };
        }


        // Post api/values/export2teslamate
        [HttpPost("export2teslamate", Name = "export2teslamate")]
        // public ActionResult<Models.CounterData> GetCounter()
        public ActionResult<string> export2teslamate(string Command)
        {
            string retorno = string.Empty;
            if (Models.Tools.ValidateToken(HttpContext.Request.Headers["Authorization"][0]))
            {
                var objInflux = new InfluxDataCollector();
                var result = objInflux.ReadDataFromInflux();
            }
            else
            {
                return BadRequest("Token not valid");
            }
            return retorno;
        }

        // Post api/values/sendcommand
        [HttpPost("sendCommand", Name = "sendCommand")]
        // public ActionResult<Models.CounterData> GetCounter()
        public ActionResult<string> sendCommand(string Command)
        {
            if (Models.Tools.ValidateToken(HttpContext.Request.Headers["Authorization"][0]))
            {
                return Models.Tools.SendData2OVMS(Command);
            }
            else
            {
                return BadRequest("Token not valid");
            }
        }

        // GET api/values/updatechargestate
        [HttpPost("updatechargestate", Name = "updatechargestate")]
        public ActionResult<string> UpdateChargeState(string state)
        {
            if (Models.Tools.ValidateToken(HttpContext.Request.Headers["Authorization"][0]))
            {
                var Payload = state;
                // Mandar por MQTT el estado
                var Topic = Program.AppConfig.MQTT_BASETOPIC + "v/c/state";
                try
                {
                    Models.Tools.MqttPublish(Topic, Payload);
                }
                catch (Exception ex)
                {
                    Models.Tools.guardarLog("MQTT Server could publish: " + ex.Message);
                }
                return Ok("State Received: " + state);
            }
            else
            {
                return BadRequest("Token not valid");
            }
        }

        // GET api/values/getcounter
        [HttpGet("GetCounter", Name = "GetCounter")]
        // public ActionResult<Models.CounterData> GetCounter()
        public ActionResult<string> GetCounter()
        {
            if (Models.Tools.ValidateToken(HttpContext.Request.Headers["Authorization"][0]))
            {
                Models.CounterData Counter = new Models.CounterData();
                // Read File Date
                Counter.LastUpdate = Models.Tools.getCounterFileDate();
                // Read Counter
                Counter.Total = Models.Tools.GetCounter();

                TimeSpan Interval = DateTime.Now - Counter.LastUpdate;
                Counter.LastUpdateInSeconds = Convert.ToInt32(Interval.TotalSeconds);

                // Solo para depuración
                //if (Program.AppConfig.DebugMode)
                //{
                //    Models.Tools.guardarLog(DateTime.Now.ToString() + " - " + Environment.MachineName);
                //}

                return string.Format("Estado de la batería: {0}%. Última actualización hace: {1} {2}",
                    Models.PreviousData.getSOC(),
                    Counter.LastUpdateInSeconds < 60 ? Counter.LastUpdateInSeconds : Convert.ToInt32(Counter.LastUpdateInSeconds / 60),
                    Counter.LastUpdateInSeconds < 60 ? "segundos." : "minutos.");
            }
            else
            {
                return BadRequest("Token not valid");
            }
        }

        // GET api/values/0
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id, string tlm, string isActive)
        {
            bool Estado = isActive == "on";

            if (id != 0)
            {
                return BadRequest("ID. not valid");
            }

            // Serialize JSON
            var objTLM = Models.Tools.parseTLM(tlm);

            if (objTLM == null)
            {
                return BadRequest("Cannot parse received TLM");
            }

            // Send Data 
            int Counter = 0;
            //int Counter = Models.Tools.SaveAndSendData(tlm);

            // Call HomeAssistant only if Estado is different
            if (Program.carState.ShouldSend2ABRP != Estado)
            {
                //msg.AppendLine(Models.Tools.SerializeReturnTLM(objTLM));
                // In order to see how we send to ABRP - Reparse TLM
                //string stringTLMParameter = serializeReturnTLM(new returnTLM(objTLM));
                Models.Tools.updateABRPSensorOnHA(Estado);
            }

            // Reset Previous Data when is stopping sending
            if (!Estado)
            {
                Models.PreviousData.resetData();
            }
            return Ok(Counter);
        }


    }
}
