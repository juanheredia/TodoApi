using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using InfluxDB.Client;
using System.Collections.Generic;
using System;
using System.Text;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using InfluxDB.Client.Writes;
using System.Linq;
namespace TodoApi.Controllers
{
    public class InfluxDataCollector : Controller
    {
        private ImportDates importDates;
        private string configFile;

        public InfluxDataCollector()
        {
            configFile = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "importdates.json");
        }
        private void ReadConfigImport()
        {
            using (StreamReader r = new StreamReader(configFile))
            {
                string json = r.ReadToEnd();
                importDates = JsonConvert.DeserializeObject<ImportDates>(json);
                // Transform Dates to localtime
                importDates.Start = new DateTime(importDates.Start.Year, importDates.Start.Month, importDates.Start.Day, importDates.Start.Hour, importDates.Start.Minute, importDates.Start.Second, DateTimeKind.Local);
                // importDates.Stop = new DateTime(importDates.Stop.Year, importDates.Stop.Month, importDates.Stop.Day, importDates.Stop.Hour, importDates.Stop.Minute, importDates.Stop.Second, DateTimeKind.Local);
                // Stop should be yesterday
                DateTime YesterdayMidNight = DateTime.Today.AddSeconds(-1);
                importDates.Stop = YesterdayMidNight;
            }
        }

        private void SaveConfigImport()
        {
            string strImportDate = JsonConvert.SerializeObject(importDates);
            Models.Tools.SaveTextInFile(strImportDate, configFile);
        }
        public IActionResult Index()
        {
            return View("Index", new Models.TeslaMate());
        }

        [HttpPost]
        public IActionResult Save()
        {
            var result = ReadDataFromInflux();
            return View("Success");
        }

        [HttpPost]
        public IActionResult Rewrite()
        {
            //var result = GenerateDataFromInfuxHA();
            //var result = RewriteDataFromInfuxToRecalculateValues();
            return View("Success");
        }

        public async Task<IActionResult> GenerateDataFromInfuxHA()
        {
            var options = new InfluxDBClientOptions.Builder()
                            .Url(Program.AppConfig.InfluxDBServer)
                            .AuthenticateToken(Program.AppConfig.InfluxDBToken.ToCharArray())
                            .ReadWriteTimeOut(TimeSpan.FromSeconds(120))
                            .TimeOut(TimeSpan.FromSeconds(120))
                            .Build();
            var influxDBClient = InfluxDBClientFactory.Create(options);
            // first ovms = 2020-07-29 
            ImportDates importDates = new ImportDates();
            importDates.Start = new DateTime(2019, 10, 10, 00, 00, 00, DateTimeKind.Local);
            importDates.Stop = new DateTime(2021, 09, 30, 23, 59, 59, DateTimeKind.Local);

            //importDates.Stop = new DateTime(2021, 04, 27, 23, 59, 59, DateTimeKind.Local);
            int TotalDays = Convert.ToInt32((importDates.Stop.Date - importDates.Start.Date).TotalDays);
            int n = 1;
            do
            {
                importDates.Stop = importDates.Start.AddDays(1).AddSeconds(-1);
                var msgDates = importDates.Start.ToString() + " | " + importDates.Stop.ToString();
                var flux = "from(bucket: \"homeassistant/autogen\")" +
                            " |> range(start: " + importDates.startEpoch.ToString() + ", stop: " + importDates.stopEpoch.ToString() + ") " +
                            " |> filter(fn: (r) => r[\"entity_id\"] == \"ovms_v_b_soc\") " +
                            " |> filter(fn: (r) => r[\"_field\"] == \"value\")";
                var fluxTables = await influxDBClient.GetQueryApi().QueryAsync(flux, "INFOINNOVA");
                var nRecords = 0;
                try
                {
                    // Fill Object with Data
                    var lobjTLM = new List<InfluxTLM>();

                    // Create empty object to fill later
                    if (fluxTables.Count > 0)
                    {
                        foreach (var t in fluxTables)
                        {
                            var r = t.Records[0];
                            if (r.GetField() == "value")
                            {
                                nRecords = t.Records.Count;
                                break;
                            }
                        }

                        for (int i = 0; i < nRecords; i++)
                        {
                            lobjTLM.Add(new InfluxTLM());
                        }
                    }

                    fluxTables.ForEach(fluxTable =>
                    {
                        var fluxRecords = fluxTable.Records;
                        int i = 0;
                        fluxRecords.ForEach(fluxRecord =>
                        {
                            var tlm = lobjTLM[i];
                            tlm.soc = Convert.ToDouble(fluxRecord.GetValue());
                            i++;
                        });
                    });

                    // Now, modify the record to save
                    //foreach (var tlm in lobjTLM)
                    //{
                    //    var percentUtil = Program.AppConfig.CAR_BATTERY * tlm.soh / 100;
                    //    tlm.distance = Models.PreviousData.getDistance(tlm.lat, tlm.lon, tlm.alt);
                    //    tlm.consumptionkwh = Models.PreviousData.getDiffSOC(tlm.soc) * percentUtil / 100;
                    //    tlm.is_aux_recuperation = tlm.is_charging && tlm.current > 0;
                    //}

                    //// Now the record back
                    //if (lobjTLM.Count > 0)
                    //{
                    //    SaveDataIntoInfluxDB(lobjTLM);
                    //}
                }
                catch (System.Exception ex)
                {
                    Models.Tools.guardarLog("Error on RewriteDataFromInfux: " + msgDates + " | " + ex.Message);
                    break;
                }
                Models.Tools.guardarLog("RewriteDataFromInfux Finished: " + msgDates + " | Records: " + nRecords.ToString());
                importDates.Start = importDates.Stop.AddSeconds(1);
                n++;
            } while (n <= TotalDays);

            return View("Index");
        }
        public async Task<IActionResult> RewriteDataFromInfuxToRecalculateValues()
        {
            var options = new InfluxDBClientOptions.Builder()
                            .Url(Program.AppConfig.InfluxDBServer)
                            //.Url("http://173.212.202.14:58086")
                            .AuthenticateToken(Program.AppConfig.InfluxDBToken.ToCharArray())
                            .ReadWriteTimeOut(TimeSpan.FromSeconds(120))
                            .TimeOut(TimeSpan.FromSeconds(120))
                            .Build();
            var influxDBClient = InfluxDBClientFactory.Create(options);

            // first ovms = 2020-07-29 

            ImportDates importDates = new ImportDates();
            importDates.Start = new DateTime(2021, 12, 11, 09, 11, 50, DateTimeKind.Local);
            importDates.Stop = new DateTime(2021, 12, 11, 09, 20, 57, DateTimeKind.Local);

            int TotalDays = Convert.ToInt32((importDates.Stop.Date - importDates.Start.Date).TotalDays);
            int n = 1;
            do
            {
                //importDates.Stop = importDates.Start.AddDays(1).AddSeconds(-1);  // this is for a complete day. 
                var msgDates = importDates.Start.ToString() + " | " + importDates.Stop.ToString();
                var flux = "from(bucket: \"homeassistant/autogen\")" +
                            " |> range(start: " + importDates.startEpoch.ToString() + ", stop: " + importDates.stopEpoch.ToString() + ") " +
                            " |> filter(fn: (r) => r[\"_measurement\"] == \"ovms\")";
                var fluxTables = await influxDBClient.GetQueryApi().QueryAsync(flux, "INFOINNOVA");
                var nRecords = 0;
                try
                {
                    // Fill Object with Data
                    var lobjTLM = new List<InfluxTLM>();

                    // Create empty object to fill later
                    if (fluxTables.Count > 0)
                    {
                        //nRecords = fluxTables[0].Records.Count;
                        foreach (var t in fluxTables)
                        {
                            var r = t.Records[0];
                            if (r.GetField() == "utc")
                            {
                                nRecords = t.Records.Count;
                                break;
                            }
                        }

                        for (int i = 0; i < nRecords; i++)
                        {
                            lobjTLM.Add(new InfluxTLM());
                        }
                    }

                    fluxTables.ForEach(fluxTable =>
                    {
                        var fluxRecords = fluxTable.Records;
                        int i = 0;
                        fluxRecords.ForEach(fluxRecord =>
                        {
                            var tlm = lobjTLM[i];
                            if (fluxRecord.GetField() == "alt") tlm.alt = Convert.ToDouble(fluxRecord.GetValue());
                            else if (fluxRecord.GetField() == "batt_temp") tlm.batt_temp = Convert.ToDouble(fluxRecord.GetValue());
                            else if (fluxRecord.GetField() == "consumptionkwh") tlm.consumptionkwh = Convert.ToDouble(fluxRecord.GetValue());
                            else if (fluxRecord.GetField() == "current") tlm.current = Convert.ToDouble(fluxRecord.GetValue());
                            else if (fluxRecord.GetField() == "distance") tlm.distance = Convert.ToDouble(fluxRecord.GetValue());
                            else if (fluxRecord.GetField() == "ext_temp") tlm.ext_temp = Convert.ToDouble(fluxRecord.GetValue());
                            else if (fluxRecord.GetField() == "is_aux_recuperation") tlm.is_aux_recuperation = Convert.ToBoolean(fluxRecord.GetValue());
                            else if (fluxRecord.GetField() == "is_charging") tlm.is_charging = Convert.ToBoolean(fluxRecord.GetValue());
                            else if (fluxRecord.GetField() == "lat") tlm.lat = Convert.ToDouble(fluxRecord.GetValue());
                            else if (fluxRecord.GetField() == "lon") tlm.lon = Convert.ToDouble(fluxRecord.GetValue());
                            else if (fluxRecord.GetField() == "power") tlm.power = Convert.ToDouble(fluxRecord.GetValue());
                            else if (fluxRecord.GetField() == "soc") tlm.soc = Convert.ToDouble(fluxRecord.GetValue());
                            else if (fluxRecord.GetField() == "soh") tlm.soh = Convert.ToDouble(fluxRecord.GetValue());
                            else if (fluxRecord.GetField() == "speed") tlm.speed = Convert.ToInt32(fluxRecord.GetValue());
                            else if (fluxRecord.GetField() == "voltaje" || fluxRecord.GetField() == "voltage")
                            {
                                var aux = Convert.ToDouble(fluxRecord.GetValue());
                                if (aux > 0) tlm.voltage = aux;
                            }

                            else if (fluxRecord.GetField() == "utc") tlm.utc = Convert.ToInt32(fluxRecord.GetValue());
                            i++;
                        });
                    });

                    // Now, modify the record to save
                    foreach (var tlm in lobjTLM)
                    {
                        //var percentUtil = Program.AppConfig.CAR_BATTERY * tlm.soh / 100;
                        //tlm.distance = Models.PreviousData.getDistance(tlm.lat, tlm.lon, tlm.alt);
                        tlm.distance = 0;
                        tlm.speed = 0;
                        //tlm.consumptionkwh = Models.PreviousData.getDiffSOC(tlm.soc) * percentUtil / 100;
                        //tlm.is_aux_recuperation = tlm.is_charging && tlm.current > 0;
                        //tlm.is_aux_recuperation = true;
                        //tlm.is_charging = true;
                    }

                    // Now the record back
                    if (lobjTLM.Count > 0)
                    {
                        SaveDataIntoInfluxDB(lobjTLM);
                    }
                }
                catch (System.Exception ex)
                {
                    Models.Tools.guardarLog("Error on RewriteDataFromInfux: " + msgDates + " | " + ex.Message);
                    break;
                }
                Models.Tools.guardarLog("RewriteDataFromInfux Finished: " + msgDates + " | Records: " + nRecords.ToString());
                importDates.Start = importDates.Stop.AddSeconds(1);
                n++;
            } while (n <= TotalDays);
            return View("Index");
        }

        public async Task<IActionResult> ReadDataFromInflux()
        {
            string FileNameToExport;

            ReadConfigImport();

            var options = new InfluxDBClientOptions.Builder()
                .Url(Program.AppConfig.InfluxDBServer)
                .AuthenticateToken(Program.AppConfig.InfluxDBToken.ToCharArray())
                .ReadWriteTimeOut(TimeSpan.FromSeconds(120))
                .TimeOut(TimeSpan.FromSeconds(120))
                .Build();
            var influxDBClient = InfluxDBClientFactory.Create(options);

            // Telemetry
            var listTM = new List<Models.TeslaMate>();

            // do it day by day
            int TotalDays = Convert.ToInt32((importDates.Stop.Date - importDates.Start.Date).TotalDays);

            if (TotalDays < 0)
            {
                return View("Nothing to Export");
            }

            importDates.EstimatedOdometer = importDates.EstimatedOdometer / 1.609; // Begin in Miles
            int n = 0;
            int id = 1; // ID for Telemetry

            // Aux var to save first record for later calculations
            var prevTM = new Models.TeslaMate();

            do
            {
                // importDates.Stop = importDates.Start.AddDays(1).AddSeconds(-1);
                // Hasta la media noche de ese mismo día
                importDates.Stop = importDates.Start.Date.AddDays(1).AddSeconds(-1);
                var flux =
                                "A = (from(bucket: \"homeassistant/autogen\") " +
                                "  |> range(start: " + importDates.startEpoch.ToString() + ", stop: " + importDates.stopEpoch.ToString() + ") " +
                                "  |> filter(fn: (r) => r[\"_measurement\"] == \"ovms\") " +
                                "  |> filter(fn: (r) =>  " +
                                "       r[\"_field\"] == \"is_aux_recuperation\"  " +
                                "    or r[\"_field\"] == \"is_charging\"  " +
                                "  ) " +
                                "  |> drop(columns: [\"_measurement\"]) " +
                                "  |> pivot( " +
                                "    rowKey:[\"_time\"], " +
                                "    columnKey: [\"_field\"], " +
                                "    valueColumn: \"_value\" " +
                                "  ) " +
                                "  |> map(fn: (r) => ({r with  " +
                                "      is_aux_recuperation: if r.is_aux_recuperation then 1.0 else 0.0, " +
                                "      is_charging: if r.is_charging then 1.0 else 0.0, " +
                                "    })) " +
                                "  ) " +
                                " " +
                                " " +
                                "B = (from(bucket: \"homeassistant/autogen\") " +
                                "  |> range(start: " + importDates.startEpoch.ToString() + ", stop: " + importDates.stopEpoch.ToString() + ") " +
                                "  |> filter(fn: (r) => r[\"_measurement\"] == \"ovms\") " +
                                "  |> filter(fn: (r) =>  " +
                                "       r[\"_field\"] == \"alt\"  " +
                                "    or r[\"_field\"] == \"batt_temp\"  " +
                                "    or r[\"_field\"] == \"consumptionkwh\"  " +
                                "    or r[\"_field\"] == \"current\"  " +
                                "    or r[\"_field\"] == \"distance\"  " +
                                "    or r[\"_field\"] == \"ext_temp\"  " +
                                "       or r[\"_field\"] == \"lat\"  " +
                                "    or r[\"_field\"] == \"lon\"  " +
                                "    or r[\"_field\"] == \"power\"  " +
                                "    or r[\"_field\"] == \"soc\"  " +
                                "    or r[\"_field\"] == \"soh\"  " +
                                "    or r[\"_field\"] == \"speed\"  " +
                                "    or r[\"_field\"] == \"utc\"  " +
                                "    or r[\"_field\"] == \"voltaje\" " +
                                "  ) " +
                                "  |> drop(columns: [\"_measurement\"]) " +
                                "  |> pivot( " +
                                "    rowKey:[\"_time\"], " +
                                "    columnKey: [\"_field\"], " +
                                "    valueColumn: \"_value\" " +
                                "  ) " +
                                ") " +
                                " " +
                                "join(tables: {A: A, B: B}, on: [\"_time\"]) " +
                                "|> map(fn: (r) => ({ " +
                                //"  Date: r._time, " +
                                //"  utc: r.utc, " +
                                "  _time: r._time, " +
                                "  display_name: r.vehicle_A, " +
                                "  battery_level: r.soc, " +
                                "  charger_voltage: r.voltaje,  " +
                                "  longitude: r.lon, " +
                                "  latitude: r.lat, " +
                                "  speed: r.speed, " +
                                "  elevation: r.alt, " +
                                "  distance: r.distance, " +
                                "  outside_temp: r.ext_temp, " +
                                "  battery_current: r.current,  " +
                                "  inside_temp: r.batt_temp, " +
                                "  power: r.power, " +
                                "  is_charging: r.is_charging,  " +
                                "  is_aux_recuperation: r.is_aux_recuperation " +
                                "})) " +
                                "|> yield(name: \"data\") ";

                try
                {
                    // Let's get more data if needed
                    var parkTime = ReadValueFromInflux("ovms_v_e_parktime", "value");
                    var Locked = ReadValueFromInflux("ovms_v_e_locked", "state");
                    var Charger_Volt = ReadValueFromInflux("ovms_v_c_voltage", "value");
                    var IdealRange = ReadValueFromInflux("ovms_v_b_range_ideal", "value");
                    var FullRange = ReadValueFromInflux("ovms_v_b_range_full", "value");
                    var EstRange = ReadValueFromInflux("ovms_v_b_range_est", "value");
                    var ChargerPilotCurrent = ReadValueFromInflux("ovms_v_c_current", "value");
                    var EstOdometer = ReadValueFromInflux("ovms_v_p_estimatedodometer", "value");

                    // Get Last Odometer from Influx else use from File 
                    //Begin in Miles
                    if (EstOdometer.Count > 0)
                    {
                        // Get just the firstvalue
                        importDates.EstimatedOdometer = Convert.ToDouble(EstOdometer[0].Records[0].GetValue()) / 1.609;
                    }

                    // Main Data
                    var fluxTables = await influxDBClient.GetQueryApi().QueryAsync(flux, "INFOINNOVA");

                    fluxTables.ForEach(fluxTable =>
                    {
                        var fluxRecords = fluxTable.Records;

                        fluxRecords.ForEach(fluxRecord =>
                        {
                            var t_is_charging = Convert.ToBoolean(fluxRecord.GetValueByKey("is_charging"));
                            var t_is_aux_recuperation = Convert.ToBoolean(fluxRecord.GetValueByKey("is_aux_recuperation"));
                            var is_charging = t_is_charging && !t_is_aux_recuperation;
                            var dateRecord = Convert.ToDateTime(fluxRecord.GetTimeInDateTime());
                            //var dateRecordUTC = Convert.ToInt32(dateRecord.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds / 1000); ;
                            var tm = new Models.TeslaMate();
                            tm.Date = "\"" + dateRecord.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss") + "\"";
                            tm.display_name = Convert.ToString(fluxRecord.GetValueByKey("display_name"));
                            var soc = Convert.ToDouble(fluxRecord.GetValueByKey("battery_level"));
                            tm.battery_level = Convert.ToInt32(soc);
                            tm.calendar_enabled = Convert.ToString(soc); //just a trick to save temp data
                            tm.charger_voltage = Convert.ToDouble(fluxRecord.GetValueByKey("charger_voltage"));
                            tm.battery_current = Convert.ToDouble(fluxRecord.GetValueByKey("battery_current"));
                            tm.longitude = Convert.ToDouble(fluxRecord.GetValueByKey("longitude"));
                            tm.latitude = Convert.ToDouble(fluxRecord.GetValueByKey("latitude"));
                            tm.speed = Convert.ToDouble(fluxRecord.GetValueByKey("speed")) / 1.609; // Converto to Millage/h
                            tm.elevation = Convert.ToDouble(fluxRecord.GetValueByKey("elevation"));
                            tm.outside_temp = Convert.ToDouble(fluxRecord.GetValueByKey("outside_temp"));
                            tm.inside_temp = Convert.ToDouble(fluxRecord.GetValueByKey("inside_temp"));
                            importDates.EstimatedOdometer += Convert.ToDouble(fluxRecord.GetValueByKey("distance")) * importDates.FixDistanceFactor / 1000 / 1.609;  // Convert to Millage 
                            tm.odometer = importDates.EstimatedOdometer;
                            tm.odometerF = tm.odometer; // Process other values (don't need nothing special or just duplicate a field)

                            tm.color = "White";
                            tm.vin = "328KSW328KSW";
                            tm.vehicle_id = 3228322832283228;
                            tm.id = 3228322832283228;
                            tm.wheel_type = "Pinwheel18";
                            tm.car_type = "model";
                            tm.car_version = "2019.36.2.1 ea322ad";
                            tm.api_version = "7";

                            tm.driver_temp_setting = tm.inside_temp;
                            tm.driver_temp_settingF = tm.inside_temp * 9 / 5 + 32;
                            tm.outside_tempF = tm.outside_temp * 9 / 5 + 32;
                            tm.inside_tempF = tm.driver_temp_settingF;
                            tm.usable_battery_level = tm.battery_level;
                            tm.charge_port_door_open = is_charging ? "TRUE" : "FALSE";
                            tm.managed_charging_active = "FALSE";
                            tm.charge_port_latch = is_charging ? "Disengaged" : "Engaged";

                            // if power is 0, see if driving or charging
                            tm.power = Convert.ToInt32(fluxRecord.GetValueByKey("power"));
                            //if (power != 0 || tm.speed > 0 || is_charging) tm.power = power;
                            tm.charger_power = tm.power;

                            // Calculate more states with some decitions
                            tm.charging_state = getChargingState(tm, is_charging);
                            tm.locked = getLocked(Locked, dateRecord);
                            tm.charger_voltage = Convert.ToDouble(GetValue(Charger_Volt, dateRecord));
                            tm.ideal_battery_range = Convert.ToDouble(GetValue(IdealRange, dateRecord));
                            tm.est_battery_range = Convert.ToDouble(GetValue(EstRange, dateRecord));
                            tm.maxRange = Convert.ToDouble(GetValue(FullRange, dateRecord));
                            tm.charger_pilot_current = Convert.ToString(GetValue(ChargerPilotCurrent, dateRecord));
                            if (id == 1) prevTM = tm.ShallowCopy(); // Just to save the first record
                            tm.data_id = id++;
                            listTM.Add(tm);
                        });
                    });
                }
                catch (Exception ex)
                {
                    Models.Tools.guardarLog("Error on collecting data: " + ex.Message);
                }
                Models.Tools.guardarLog(string.Format("Finished from {0} to {1}", importDates.Start, importDates.Stop));

                importDates.Start = importDates.Stop.AddSeconds(1);
                n++;
            } while (n <= TotalDays);

            // Tabla completa del rango, recorremos todos los registros para continuar rellenando datos
            // Para agrupar las cargas
            var ChargeGroup = 0;
            DateTime lastCharge = DateTime.MinValue;
            int idList = 0;
            foreach (var r in listTM)
            {
                // Diferencia de tiempo para calculos de estado y cambios
                double TimeInSeconds = (DateTime.Parse(r.Date.Replace("\"", string.Empty)) - DateTime.Parse(prevTM.Date.Replace("\"", string.Empty))).TotalSeconds;


                // Fix Ideal Range
                r.ideal_battery_range = (r.ideal_battery_range <= 0) ? prevTM.ideal_battery_range : r.ideal_battery_range;
                prevTM.ideal_battery_range = r.ideal_battery_range;

                r.battery_range = r.ideal_battery_range;

                // Fix Estimated Range
                r.est_battery_range = (r.est_battery_range <= 0) ? prevTM.est_battery_range : r.est_battery_range;
                prevTM.est_battery_range = r.est_battery_range;

                // Fix Max Range
                r.maxRange = (r.maxRange <= 0) ? prevTM.maxRange : r.maxRange;
                prevTM.maxRange = r.maxRange;

                // Fix charger_voltage
                r.charger_voltage = (r.charger_voltage == 0) ? prevTM.charger_voltage : r.charger_voltage;
                prevTM.charger_voltage = r.charger_voltage;

                if (r.charging_state == "Charging")
                {
                    // Acumulamos y aprovechamos en agrupar las cargas para luego sacar la media y detenerminar el tipo de cargador.
                    // nos apoyamos en la variable que no se usa nunca "pr"
                    // As a trick real battery_current is on calendar_enabled
                    double diffSoc = Convert.ToDouble(r.calendar_enabled) - Convert.ToDouble(prevTM.calendar_enabled);
                    double charge_energy_added = Convert.ToDouble(Program.AppConfig.CAR_BATTERY * diffSoc / 100);
                    prevTM.charge_energy_added = Convert.ToString(Convert.ToDouble(prevTM.charge_energy_added) + charge_energy_added); // Acumula
                    r.charge_energy_added = Convert.ToString(prevTM.charge_energy_added);
                    // si ha transcurrido mucho tiempo desde la última carga
                    if ((DateTime.Parse(r.Date.Replace("\"", string.Empty)) - lastCharge).TotalSeconds > 610)
                    {
                        ChargeGroup++; // Generamos nuevo grupo  
                    }
                    r.pr = Convert.ToString(ChargeGroup);
                    lastCharge = DateTime.Parse(r.Date.Replace("\"", string.Empty));
                }
                else
                {
                    // Solo asignamos el acumulado
                    r.charge_energy_added = Convert.ToString(prevTM.charge_energy_added);
                }
                prevTM.calendar_enabled = r.calendar_enabled;
                prevTM.battery_current = r.battery_current;

                // Calculate states
                if (r.charging_state == "Charging" || r.speed > 0)
                {
                    r.state = "online";
                }
                else if (TimeInSeconds > (1 * 60 * 60)) // More than 1 hour
                {
                    r.state = "asleep";
                }
                else if (TimeInSeconds > (10 * 60)) // More than 10 minutes
                {
                    r.state = "offline";
                }
                else // If has any other value because it's reporting
                {
                    r.state = r.state = "online";
                }

                // In case or driving or stopped
                if (r.charging_state == "Charging")
                {
                    r.shift_state = "P";
                }
                else if (r.speed > 0)
                {
                    r.shift_state = "D";
                }
                else
                {
                    if (TimeInSeconds > (2 * 60)) // Si pasa más de 2 minutos
                    {
                        r.shift_state = "P";
                        // deberíamos retroceder y poner en P a todos los que su velocidad es 0

                        if (idList > 0)
                        {
                            int aux = idList - 1;
                            while (listTM[aux].speed == 0 && listTM[aux].shift_state == "D")
                            {
                                listTM[aux].shift_state = "P";
                                aux--;
                            }
                        }
                    }
                    else
                    {
                        r.shift_state = (string.IsNullOrEmpty(prevTM.shift_state)) ? "None" : prevTM.shift_state;
                    }
                }
                prevTM.charging_state = r.charging_state;
                prevTM.shift_state = r.shift_state;
                prevTM.Date = r.Date;

                //r.calendar_enabled = (r.state == "online") ? "TRUE" : string.Empty;
                r.calendar_enabled = string.Empty;
                idList++;
            }

            if (listTM.Count > 0)
            {
                //var lCargas
                var maxChargeGroup = listTM.Max(r => Convert.ToInt32(r.pr));
                for (int i = 1; i <= maxChargeGroup; i++)
                {
                    // Media del grupo
                    var Group = listTM.Where(r => Convert.ToInt32(r.pr) == i);
                    double MeanPower = Group.Average(r => r.power);
                    Group.ToList().ForEach(r => r.fast_charger_type = (MeanPower > -7 && MeanPower <= 0) ? "MCSingleWireCAN" : "Combo");
                    Group.ToList().ForEach(r => r.charger_phases = (r.fast_charger_type == "Combo") ? "None" : "1");
                }

                // Fix pr 
                listTM.ToList().ForEach(r => r.pr = string.Empty);

                // Fix Date to easy import on Teslamate, rest 10 years
                foreach (var r in listTM)
                {
                    var dateRecord = DateTime.Parse(r.Date.Replace("\"", string.Empty)).AddYears(-10);
                    r.Date = "\"" + dateRecord.ToString("yyyy-MM-dd HH:mm:ss") + "\"";
                }

                // let's Delete all csv files
                // foreach (string sFile in System.IO.Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.csv"))
                // {
                //     System.IO.File.Delete(sFile);
                // }

                DateTime lastRecordDateTime = DateTime.Parse(prevTM.Date.Replace("\"", string.Empty));
                FileNameToExport = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TeslaFi" + lastRecordDateTime.ToString("yyMMdd") + ".csv");

                // Write to file
                ExportData.ExportCsv(listTM, FileNameToExport);
                Models.Tools.guardarLog("Finishing collecting data, saved on file: " + FileNameToExport);
            }

            // Save last Odometer and Dates
            importDates.Start = importDates.Stop.AddSeconds(1);
            importDates.Stop = importDates.Start.AddMonths(1).AddSeconds(-1); // De momento un mes más. Aunque por código luego estamos solo considerando la medianoche del día anterior
            importDates.EstimatedOdometer = importDates.EstimatedOdometer * 1.609; // Store in Kms
            SaveConfigImport();
            return View("Success");
        }

        private string getChargingState(Models.TeslaMate tm, bool is_charging)
        {
            string Value = string.Empty;
            if (is_charging)
            {
                Value = Convert.ToDouble(tm.power) < 0 ? "Charging" : "NoPower";
            }
            else
            {
                Value = (Convert.ToInt32(tm.speed) > 0) ? "Disconnected" : "Complete";
            }
            return Value;
        }

        private string getLocked(List<InfluxDB.Client.Core.Flux.Domain.FluxTable> fluxTables, DateTime t)
        {
            var r = Convert.ToString(getRecord(fluxTables, t));
            if (r == "no")
                r = "FALSE";
            else if (r == "yes")
                r = "TRUE";
            return r;
        }

        private int GetValue(List<InfluxDB.Client.Core.Flux.Domain.FluxTable> fluxTables, DateTime t)
        {
            var r = Convert.ToInt32(getRecord(fluxTables, t));
            return r;
        }

        private List<InfluxDB.Client.Core.Flux.Domain.FluxTable> ReadValueFromInflux(string entity_id, string _field)
        {
            var options = new InfluxDBClientOptions.Builder()
                .Url(Program.AppConfig.InfluxDBServer)
                .AuthenticateToken(Program.AppConfig.InfluxDBToken.ToCharArray())
                .ReadWriteTimeOut(TimeSpan.FromSeconds(60))
                .TimeOut(TimeSpan.FromSeconds(60))
                .Build();
            var influxDBClient = InfluxDBClientFactory.Create(options);
            //var influxDBClient = InfluxDBClientFactory.Create(Program.AppConfig.InfluxDBServer, Program.AppConfig.InfluxDBToken);
            var flux =
                "from(bucket: \"homeassistant/autogen\") " +
                "  |> range(start: " + importDates.startEpoch.ToString() + ", stop: " + importDates.stopEpoch.ToString() + ") " +
                "  |> filter(fn: (r) => r[\"entity_id\"] == \"" + entity_id + "\") " +
                "  |> filter(fn: (r) => r[\"_field\"] == \"" + _field + "\")";
            return influxDBClient.GetQueryApiSync().QuerySync(flux, "INFOINNOVA"); ;
        }

        private object getRecord(List<InfluxDB.Client.Core.Flux.Domain.FluxTable> fluxTables, DateTime t)
        {
            object retorno = null;
            fluxTables.ForEach(fluxTable =>
                {
                    var fluxRecords = fluxTable.Records;
                    var fluxRecord = fluxRecords.FindLast(r => r.GetTimeInDateTime() >= t.AddMinutes(-1) && r.GetTimeInDateTime() <= t);
                    if (fluxRecord != null)
                        retorno = fluxRecord.GetValue();
                });
            return retorno;
        }

        public static void SaveDataIntoInfluxDB(List<InfluxTLM> lobjTLM)
        {
            string Token = Program.AppConfig.InfluxDBToken;
            var influxDBClient = InfluxDBClientFactory.Create(Program.AppConfig.InfluxDBServer, Token);

            using (var writeApi = influxDBClient.GetWriteApi())
            {
                foreach (var objTLM in lobjTLM)
                {
                    var timeFromOVMS = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(objTLM.utc * 1000.0);
                    var point = PointData.Measurement("ovms")
                    .Tag("vehicle", "konaev")
                    .Field("alt", objTLM.alt)
                    .Field("batt_temp", objTLM.batt_temp)
                    .Field("current", objTLM.current)
                    .Field("ext_temp", objTLM.ext_temp)
                    .Field("is_charging", objTLM.is_charging)
                    .Field("lat", objTLM.lat)
                    .Field("lon", objTLM.lon)
                    .Field("power", objTLM.power)
                    .Field("soc", objTLM.soc)
                    .Field("soh", objTLM.soh)
                    //.Field("speed", Convert.ToDouble(objTLM.speed))
                    .Field("speed", objTLM.speed)
                    .Field("utc", objTLM.utc)
                    .Field("distance", objTLM.distance)
                    .Field("consumptionkwh", objTLM.consumptionkwh)
                    .Field("voltage", objTLM.voltage)
                    .Field("is_aux_recuperation", objTLM.is_aux_recuperation)
                    .Timestamp(timeFromOVMS, InfluxDB.Client.Api.Domain.WritePrecision.Ns);
                    writeApi.WritePoint(Program.AppConfig.InfluxDBDataBase, Program.AppConfig.InfluxDBUser, point);
                }
            }
            influxDBClient.Dispose();
        }
    }

    public static class ExportData
    {
        public static void ExportCsv<T>(List<T> genericList, string fileName)
        {
            var sb = new StringBuilder();

            var header = "";
            var info = typeof(T).GetProperties();
            if (!File.Exists(fileName))
            {
                var file = File.Create(fileName);
                file.Close();
                foreach (var prop in typeof(T).GetProperties())
                {
                    header += prop.Name + ",";
                }
                //header = header.Substring(0, header.Length - 1);
                header = header.Remove(header.Length - 1);
                sb.AppendLine(header);
                TextWriter sw = new StreamWriter(fileName, true);
                sw.Write(sb.ToString());
                sw.Close();
            }
            foreach (var obj in genericList)
            {
                sb = new StringBuilder();
                var line = "";
                foreach (var prop in info)
                {
                    line += prop.GetValue(obj, null) + ",";
                }
                //line = line.Substring(0, line.Length - 1);
                line = line.Remove(line.Length - 1);
                sb.AppendLine(line);
                TextWriter sw = new StreamWriter(fileName, true);
                sw.Write(sb.ToString());
                sw.Close();
            }
        }
    }

    public class ImportDates
    {
        public double EstimatedOdometer { get; set; }
        public double FixDistanceFactor { get; set; }
        public DateTime Start { get; set; }
        public DateTime Stop { get; set; }
        [JsonIgnore]
        public int startEpoch
        {
            get
            {
                TimeSpan t = Start.ToUniversalTime() - new DateTime(1970, 1, 1);
                return (int)t.TotalSeconds;
            }
        }
        [JsonIgnore]
        public int stopEpoch
        {
            get
            {
                TimeSpan t = Stop.ToUniversalTime() - new DateTime(1970, 1, 1);
                return (int)t.TotalSeconds;
            }
        }
    }

    public class InfluxTLM
    {
        public double alt { get; set; }
        public double batt_temp { get; set; }
        public double current { get; set; }
        public double ext_temp { get; set; }
        public bool is_charging { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
        public double power { get; set; }
        public double soc { get; set; }
        public double soh { get; set; }
        public int speed { get; set; }
        public int utc { get; set; }
        public double distance { get; set; }
        public double consumptionkwh { get; set; }
        public double voltage { get; set; }
        public bool is_aux_recuperation { get; set; }

    }

}