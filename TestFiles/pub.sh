SERVER=$1
USER=$2
PASS=$3
BASETOPIC=$4
for (( i=1; i<=20; ++i)); do
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/p/odometer" -m "0" &
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/e/on" -m "no"&
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/p/latitude" -m "40.${i}6458" &
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/p/longitude" -m "-3.67613" &
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/p/altitude" -m "512.7" &
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/b/soc" -m "50" &
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/b/soh" -m "100" &
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/e/temp" -m "25.5" &
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/b/temp" -m "27.6" &
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/b/voltage" -m "368.5" &
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/b/current" -m "20" &
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/b/power" -m "${i}" &
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/c/state" -m "Driving" &
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/p/speed" -m "1${i}" &
    mosquitto_pub -u $USER -P $PASS -h $SERVER -t "$BASETOPIC/v/e/parktime" -m "${i}" &
    sleep 5
done
